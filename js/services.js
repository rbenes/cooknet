angular.module('CooknetService', ['ngResource'])
  .factory('CooknetResource', function($resource) {
    
    var api = $resource(
      'api/recipe/:param1/:param2',
      {
        'param1': ''
      , 'param2': ''
      }, {
        'update': { 'method': 'PUT' }
      }
    );

    return api;            
  });