Ctrl = (function() {
  "use strict";

  function Ctrl() {}

  Ctrl.prototype.List = function($scope, CooknetResource) {
    $scope.recipes = CooknetResource.query();
  }

  Ctrl.prototype.New = function($scope, $location, CooknetResource) {
          $scope.recipes = new CooknetResource({
                  "id":0, "recipe_name":'', 
                  "cooking_process":'', "url":''
          });
          
          $scope.save = function() {
                  console.log('save button');
                  
                  $scope.recipes.$save(function(res) {
                          $location.path('/');
                  });
          }
  }
  
  Ctrl.prototype.Edit = function($scope, $routeParams, $window, $location, CooknetResource) {
          CooknetResource.get({param1: $routeParams.id}, function(res) {
                  $scope.recipes = res;
                  
                  console.log($scope.recipes);
          });
          
    $scope.save = function() {$scope.recipes.$update(
      {param1: $routeParams.id}, function(res) {
          $location.path('/');
      });
    }
    
    $scope.destroy = function() {
            var confirm = $window.confirm('Delete '+$scope.recipes.name+ ' recipe?');
            if(confirm) {
                    $scope.recipes.$delete({param1: $routeParams.id}, function(res) {
                            $location.path('/');
                    });
            }
    }
  }

  return Ctrl;
})();

var ctrl = new Ctrl();