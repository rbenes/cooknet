<?php

require 'rb.php';
use RedBean_Facade as R;
R::setup('mysql:host=localhost;dbname=cooknet','root','');

require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();



$app = new \Slim\Slim();

$app->get('/recipe', function () use ($app) {
	$recipes = R::getAll('select * from recipe');
    echo json_encode( $recipes );
});

$app->get('/recipe/:id', function ($id) use ($app) {
    echo json_encode( R::load('recipes', $id) );
});

$app->run();	